//
//  AddPlayerController.swift
//  CrazyGame
//
//  Created by Jonas on 31/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class AddPlayerController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var playerImage: UIImageView?
    @IBOutlet weak var playerName: UITextField!
    var imagePicker: UIImagePickerController!
    @IBOutlet weak var takePhotoBtn: UIButton!
    @IBOutlet weak var addPlayerBtn: UIButton!    
    @IBOutlet weak var photoHeight: NSLayoutConstraint!
    @IBAction func photButton(sender: AnyObject) {
        
        takePhoto()
        
    }
    
    @IBAction func addPlayerButton(sender: AnyObject) {
        
        if (playerName.text!.characters.count > 2 && playerImage != nil) {

            // name must be unique
            if (sharedClass.getArray().count > 0) {
                
                if sharedClass.getArray().contains(playerName.text!) {
                    
                    print("Choose a different name!")
                    
                } else {
                    
                    sharedClass.addImage( UIImageJPEGRepresentation(playerImage!.image!, 1.0)!, imageName:playerName.text! )
                    
                    sharedClass.addToArray(playerName.text!)
                    self.navigationController?.popViewControllerAnimated(true)
                    
                }
                
            } else {
                
                // sharedClass.addImage( UIImagePNGRepresentation(playerImage!.image!)!, imageName:playerName.text! )
                sharedClass.addImage( UIImageJPEGRepresentation(playerImage!.image!, 1.0)!, imageName:playerName.text! )
                
                sharedClass.addToArray(playerName.text!)
                self.navigationController?.popViewControllerAnimated(true)
                
            }
        
        } else {
            
            print("Too short name or no image taken")
            
        }

    }
            
    func takePhoto() {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        photoHeight.constant = playerImage!.frame.size.width
        playerImage!.layer.cornerRadius = playerImage!.frame.size.width / 2
        // playerImage!.frame.size.width
        playerImage!.image = info[UIImagePickerControllerOriginalImage] as? UIImage        
        
        /*
        // let defaults = NSUserDefaults.standardUserDefaults()
        var arr = sharedClass.getImageArray()
        
        let url = NSURL(string: arr[0])
        
        // let url = NSURL(string: defaults.stringForKey("image")!)
        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
        testImage.image = UIImage(data: data!)
        */
        
    }
    
    override func viewWillAppear(animated: Bool) {
        takePhotoBtn = sharedClass.button(takePhotoBtn)
        addPlayerBtn = sharedClass.button(addPlayerBtn)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            dispatch_async(dispatch_get_main_queue()) {
                
                self.imagePicker =  UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .Camera
                
            }
        }
        
        playerName.delegate = self
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func saveImage (image: UIImage, path: String ) -> Bool{
        
        // let pngImageData = UIImagePNGRepresentation(image)
        let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = jpgImageData!.writeToFile(path, atomically: true)
        
        return result
        
    }

}
