//
//  AddActionsControllerViewController.swift
//  CrazyGame
//
//  Created by Jonas on 31/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class AddActionsControllerViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var clearBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        actionText.delegate = self
        
    }
    
    @IBAction func clearText(sender: AnyObject) {
    
        actionText.text! = ""
    
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(animated: Bool) {
        
        actionBtn = sharedClass.button(actionBtn)
        
        clearBtn = sharedClass.button(clearBtn)
        
    }
        
    @IBOutlet weak var actionText: UITextView!
    
    @IBAction func addActionButton(sender: AnyObject) {
        
        let defaultText = "Tap to enter an action. (For example: \"Shout YOLO really high\" or \"Ask a stranger to give u a hug\")"
        
        if (actionText.text!.characters.count > 2 && actionText.text != defaultText) {
            sharedClass.addToActionsArray(actionText.text!)
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            print("Too short")
        }
    
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
