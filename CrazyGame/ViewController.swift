//
//  ViewController.swift
//  CrazyGame
//
//  Created by Jonas on 30/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var choosePlayer: UIButton!
    @IBOutlet weak var chooseActions: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // sharedClass.showFiles()
        // deleteAll()
    }

    override func viewDidLayoutSubviews() {
        // navigationController!.navigationBarHidden = false
        navigationController!.navigationBar.barTintColor = UIColor.blackColor()
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController!.navigationBar.translucent = false
        
        choosePlayer = sharedClass.button(choosePlayer)
        chooseActions = sharedClass.button(chooseActions)
        playBtn = sharedClass.button(playBtn)
        
    }
    
    @IBAction func playButton(sender: AnyObject) {
        
        if (sharedClass.getArray().count > 1 && sharedClass.getActionsArray().count > 1) {
            performSegueWithIdentifier("toGameSegue", sender: self)
        } else {
            
            let alertController = UIAlertController(title: "Ooops", message:
                "Must be at least 2 players and 2 actions, add more plz", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    func deleteAll() {
    
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(nil, forKey: "imageArray")
        defaults.setObject(nil, forKey: "actionsArray")
        defaults.setObject(nil, forKey: "playerArray")
                
        let fileManager = NSFileManager.defaultManager()
        let documentsUrl = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as NSURL
        
        do {
            // if you want to filter the directory contents you can do like this:
            if let directoryUrls = try? NSFileManager.defaultManager().contentsOfDirectoryAtURL(documentsUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsSubdirectoryDescendants) {
                print("")
                // print(directoryUrls)
                
                for i in directoryUrls {
                    
                    print(i)
                    print("")
                    
                    do {
                        try fileManager.removeItemAtURL(i)
                        
                        print("success")
                        
                    } catch {
                        
                        print("failed", error)
                        
                    }
                    
                    print("")
                    
                }
                
            }
            
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
 
        self.navigationController?.navigationBarHidden = false
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        
        let btn = UIButton(type: UIButtonType.System) as UIButton
        btn.setTitle("Test Button", forState: UIControlState.Normal)
        
        let leftNavBarButton = UIBarButtonItem(customView:btn)
        self.navigationItem.rightBarButtonItem = leftNavBarButton
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

