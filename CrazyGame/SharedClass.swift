//
//  SharedClass.swift
//  CrazyGame
//
//  Created by Jonas on 30/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import Foundation

let sharedClass = SharedClass()

class SharedClass {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    func getArray() -> [String] {
        
        if let playerArray = defaults.arrayForKey("playerArray") {
            return playerArray as! [String]
        } else {
            let playerArray = []
            return playerArray as! [String]
        }
    
    }
    
    func button(btn: UIButton) -> UIButton {
        
        btn.layer.borderColor = UIColor.whiteColor().CGColor
        btn.layer.borderWidth = 1.5
        // btn.backgroundColor = UIColor(red: 15/255, green: 52/255, blue: 213/255, alpha: 1.0)
        btn.tintColor = UIColor.whiteColor()
        btn.backgroundColor = UIColor.blackColor()
        
        return btn
        
    }
    
    func removeFromArray(index: Int) {
        var array = getArray() as [String]
        array.removeAtIndex(index-1)
        defaults.setObject(nil, forKey: "playerArray")
        defaults.setObject(array, forKey: "playerArray")
    }
    
    func addToArray(playerName: String) {
        var array = getArray() as [String]
        array.append(playerName)
        defaults.setObject(nil, forKey: "playerArray")
        defaults.setObject(array, forKey: "playerArray")
    }
    
    func getActionsArray() -> [String] {
        
        if let actionsArray = defaults.arrayForKey("actionsArray") {
            return actionsArray as! [String]
        } else {
            let actionsArray = []
            return actionsArray as! [String]
        }
    }
    
    func removeFromActionsArray(index: Int) {
        var array = getActionsArray() as [String]
        array.removeAtIndex(index-1)
        defaults.setObject(nil, forKey: "actionsArray")
        defaults.setObject(array, forKey: "actionsArray")
    }
    
    func addToActionsArray(actionText: String) {
        var array = getActionsArray() as [String]
        array.append(actionText)
        defaults.setObject(nil, forKey: "actionsArray")
        defaults.setObject(array, forKey: "actionsArray")
    }
    
    func getImages() -> [String:UIImage] {
        
        var dictWithImages = [String:UIImage]()
        
        for i in getArray() {
            dictWithImages[i] = getImage(i)
        }
        
        return dictWithImages
    }
    
    func getImage(name: String) -> UIImage {
    
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let url = documentsURL.URLByAppendingPathComponent(name + ".jpg")
        let data = NSData(contentsOfURL: url)
        
        return UIImage(data: data!)!
    
    }
    
    func addImage(imageData: NSData, imageName: String) {
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        
        let imageURL = documentsURL.URLByAppendingPathComponent(imageName + ".jpg")
        
        if !imageData.writeToURL(imageURL, atomically: false)
        {
            print("not saved")
        } else {
            print("saved")
            print(imageURL)
            
            /*
            // array with urlstrings, get array, add to it and save
            var imageArray = getImageArray()
            
            imageArray.append(String(imageURL))
            defaults.setObject(nil, forKey: "imageArray")
            defaults.setObject(imageArray, forKey: "imageArray")
            
            // defaults.setObject(String(imageURL), forKey: "image")
            */

        }
        
    }
    
    /*
    func saveImage(image: UIImage) {
        
        let fileManager = NSFileManager.defaultManager()
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        
        let filePathToWrite = "\(paths)/SaveFile.png"
        
        let imageData: NSData = UIImagePNGRepresentation(image)!
        
        fileManager.createFileAtPath(filePathToWrite, contents: imageData, attributes: nil)
        
        let getImagePath = (paths as NSString).stringByAppendingPathComponent("0.png")
        
        if (fileManager.fileExistsAtPath(getImagePath))
        {
            print("FILE AVAILABLE");
            
            //Pick Image and Use accordingly
            // let imageis: UIImage = UIImage(contentsOfFile: getImagePath)!
            
            // let data: NSData = UIImagePNGRepresentation(imageis)!
            
            // delete it
            do {
                try fileManager.removeItemAtPath(getImagePath)
                print("")
                print("success")
                print("")
            } catch {
                print("")
                print("failed", error)
                print("")
            }
            
        }
        else
        {
            print("FILE NOT AVAILABLE");
            
        }
        
    }

    */
    
    func showFiles() {
    
        let fileManager = NSFileManager.defaultManager()
        let documentsUrl = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as NSURL
        
        do {
        if let directoryUrls = try? NSFileManager.defaultManager().contentsOfDirectoryAtURL(documentsUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsSubdirectoryDescendants) {
            print(directoryUrls)
        }
            
        }
        
    
    }
    /*
    func getImageArray() -> [String] {
    
        if let imageArray = defaults.arrayForKey("imageArray") {
            return imageArray as! [String]
        } else {
            let imageArray = []
            return imageArray as! [String]
        }
        
    }
    
    func removeFromImageArray(index: Int) {
     
        var imageArray = getImageArray()
        removeImage(index - 1)
        imageArray.removeAtIndex(index - 1)
        defaults.setObject(nil, forKey: "imageArray")
        defaults.setObject(imageArray, forKey: "imageArray")
        
    }
    */
    
    func removeImage(name:String) {
        
        let fileManager = NSFileManager.defaultManager()
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        
        let getImagePath = (paths as NSString).stringByAppendingPathComponent(name + ".jpg")
        
        if (fileManager.fileExistsAtPath(getImagePath))
        {
            print("FILE AVAILABLE");
            
            // delete it
            do {
                try fileManager.removeItemAtPath(getImagePath)
                print("")
                print("success")
                print("")
            } catch {
                print("")
                print("failed", error)
                print("")
            }
            
        }
        else
        {
            print("FILE NOT AVAILABLE");
            
        }
       
    }
    
}

