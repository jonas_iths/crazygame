//
//  TehGameController.swift
//  CrazyGame
//
//  Created by Jonas on 31/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit
import AVFoundation

class TehGameController: UIViewController {
    
    @IBOutlet weak var asdasd: NSLayoutConstraint!
    @IBOutlet weak var playAgainButton: UIButton!
    @IBOutlet weak var actionField: UITextView!
    @IBOutlet weak var theImage: UIImageView!
    
    var counter = 0
    var timer = NSTimer()
    let getActionsArray = sharedClass.getActionsArray()
    let getArray = sharedClass.getArray()
    let imagesDictionary = sharedClass.getImages()
    
    var beepPlayer = AVAudioPlayer()
    var beepPlayer2 = AVAudioPlayer()
    let beepSoundURL =  NSBundle.mainBundle().URLForResource("starzinger", withExtension: "wav")!
    let beepSoundURL2 =  NSBundle.mainBundle().URLForResource("star_finish", withExtension: "wav")!
    
    @IBAction func playAgain(sender: AnyObject) {
        startGame()
    }
    
    func playMySound(choice: Int){
        
        var sound = beepSoundURL
        
        if choice == 2 {
            sound = beepSoundURL2
        }
        
        if counter % 2 == 0 {
            do
            {
                beepPlayer = try AVAudioPlayer(contentsOfURL: sound, fileTypeHint: nil)
                beepPlayer.prepareToPlay()
                beepPlayer.play()
            }
            catch {
                print("error sound")
            }
        } else {
            do
            {
                beepPlayer2 = try AVAudioPlayer(contentsOfURL: sound, fileTypeHint: nil)
                beepPlayer2.prepareToPlay()
                beepPlayer2.play()
            }
            catch {
                print("error sound")
            }
        }

    }
    
    override func viewDidLayoutSubviews() {
        
        actionField.contentInset = UIEdgeInsetsMake(0,0,0,0);
        asdasd.constant = theImage.frame.size.width
        theImage!.layer.cornerRadius = asdasd.constant / 2
        theImage!.layer.borderWidth = 1.0
        theImage!.layer.borderColor = UIColor.whiteColor().CGColor
        
        playAgainButton = sharedClass.button(playAgainButton)
        playAgainButton.alpha = 0
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startGame()
    }
    
    func startGame() {
        
        counter = 0
        actionField.text = ""
        playAgainButton.alpha = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(0.4, target: self, selector: "randomplayer", userInfo: nil, repeats: true)
        
    }
    
    func randomAction() {
        
        if (counter < 16) {
            
            let count =  UInt32(getActionsArray.count)
            var randomNumber:Int
            var stringAction:String
            
            repeat {
                randomNumber = Int(arc4random_uniform(count))
                // print(randomNumber)
                stringAction = getActionsArray[randomNumber]
            } while stringAction == actionField.text
            
            if counter == 15 {
                playMySound(2)
            } else {
                playMySound(1)
            }
            
            actionField.text = stringAction
            
        } else {
            
            timer.invalidate()
            
            // screenShotMethod()
            
            // after 3 seconds, show play again and hare on FB button
            // showButtons()
            
            UIView.animateWithDuration(1.5, animations: {
                self.playAgainButton.alpha = 0.8
            })            
            
        }
    }
    
    func randomplayer() {
        
        if (counter < 8) {
            
            let count =  UInt32(getArray.count)
            var randomNumber:Int
            var randImage:UIImage
            
            repeat {
                randomNumber = Int(arc4random_uniform(count))
                randImage = imagesDictionary[ getArray[randomNumber] ]!
            } while theImage.image == randImage
            
            theImage.image = randImage
            
            if counter == 7 {
                playMySound(2)
            } else {
                playMySound(1)
            }
            
        } else {
            randomAction()
        }
        
        // print("count: ", counter)
        counter++
    }
    
    func screenShotMethod() {
        let layer = UIApplication.sharedApplication().keyWindow!.layer
        let scale = UIScreen.mainScreen().scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
