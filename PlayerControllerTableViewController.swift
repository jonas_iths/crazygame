//
//  PlayerControllerTableViewController.swift
//  CrazyGame
//
//  Created by Jonas on 30/10/15.
//  Copyright © 2015 Jonas. All rights reserved.
//

import UIKit

class PlayerControllerTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // tableView.rowHeight = UITableViewAutomaticDimension
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1 + sharedClass.getArray().count
        
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:PlayerTableViewCell
        
        if (indexPath.row != 0) {
            
            cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PlayerTableViewCell
            cell.name.text = sharedClass.getArray()[indexPath.row - 1]
            
            cell.theImage.layer.cornerRadius = cell.theImage.frame.size.width / 2;
            cell.theImage.clipsToBounds = true;
            cell.theImage.layer.borderWidth = 1.0
            cell.theImage.layer.borderColor = UIColor.whiteColor().CGColor
            cell.theImage.image = sharedClass.getImage(sharedClass.getArray()[indexPath.row - 1])
            
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("Header", forIndexPath: indexPath) as! PlayerTableViewCell
            
            cell.Button.layer.borderColor = UIColor.whiteColor().CGColor
            cell.Button.layer.borderWidth = 1.5
            
        }        
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
      
        if indexPath.row == 0 {
            return 70
        } else {
            return 95
        }
        
        // return UITableViewAutomaticDimension
        
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == .Delete) {

            // Delete the row from the data source            
            sharedClass.removeImage(sharedClass.getArray()[indexPath.row - 1])
            sharedClass.removeFromArray(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        
        if indexPath.row == 0 {
            return false
        } else {
            return true
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        
    }

}
    